using System;

namespace Solutis.Bank.Core
{
    public class InsufficientFundsException : Exception
    {

        private static string GetMessage(decimal saldo, decimal valorSaque)
        {
            decimal diff = saldo - valorSaque;
            return $"Saldo insuficiente => Saldo: {saldo}, Valor Saque: {valorSaque}, Diferença: {diff}";
        }

        public InsufficientFundsException(decimal saldo, decimal valorSaque) : base(GetMessage(saldo, valorSaque)) { }

    }
}