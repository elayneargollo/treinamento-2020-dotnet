namespace Solutis.Bank.Core
{
    public class Banco
    {
        public string Nome { get; }

        public Banco(string nome)
        {
            this.Nome = nome;
        }
    }
}