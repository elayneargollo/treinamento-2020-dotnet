```
# Criação dos projetos

dotnet new console
dotnet new nunit

# Criação da Solution
dotnet new sln
# Adiciono os dois projetos na solution [Projeto....csproj]

dotnet sln add Projeto1....csproj
dotnet sln add Projeto2....csproj


# Adicionar duas deps para o projeto de testes

dotnet add package coverlet.collector --version 1.3.0
dotnet add package coverlet.msbuild --version 2.9.0

# Adicionar a referência do projeto que eu querop testar dentro do projeto de testes
dotnet add reference ../Projeto1/Projeto1....csproj


# Rodo os testes

## Forma fácil:
dotnet test

## Forma dom relatórios
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput="../coveragereport.opencover.xml"

reportgenerator "-reports:./coveragereport.opencover.xml" "-targetdir:coveragereport" -reporttypes:Html

# Para instalar o ReportGenerator

https://github.com/danielpalme/ReportGenerator

```