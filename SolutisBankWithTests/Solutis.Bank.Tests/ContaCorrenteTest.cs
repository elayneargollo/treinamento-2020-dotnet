using System;
using NUnit.Framework;
using Solutis.Bank.Core;

namespace Solutis.Bank.Tests
{
    public class Tests
    {
        private Agencia agencia;
        private Correntista correntista;

        [SetUp]
        public void Setup()
        {
            this.agencia = new Agencia("REDENCAO", 125, new Banco("BRADESCO"));
            this.correntista = new Correntista("Paloma");
        }

        [Test]
        public void DeveEfetuarDeposito()
        {
            Conta conta0 = new ContaCorrente();
            conta0.Numero = "233";

            conta0.Agencia = this.agencia;
            conta0.Correntista = this.correntista;

            conta0.Depositar(15.01m);

            Assert.AreEqual(conta0.Saldo, 15.01m);
        }


        [Test]
        public void DeveEfetuarSaque()
        {
            Conta conta0 = new ContaCorrente();
            conta0.Numero = "233";

            conta0.Agencia = this.agencia;
            conta0.Correntista = this.correntista;

            conta0.Depositar(15.01m);
            // Assert.AreEqual(conta0.Saldo, 15.01m);

            conta0.Sacar(12.04m);

            Assert.AreEqual(conta0.Saldo, 2.97m);
        }

        [Test]
        public void DeveEfetuarSaqueAteZerarAConta()
        {
            Conta conta0 = new ContaCorrente();
            conta0.Numero = "233";

            conta0.Agencia = this.agencia;
            conta0.Correntista = this.correntista;

            conta0.Depositar(15.01m);
            // Assert.AreEqual(conta0.Saldo, 15.01m);

            conta0.Sacar(12.04m);
            // Assert.AreEqual(conta0.Saldo, 2.97m);

            conta0.Sacar(2.97m);
            Assert.AreEqual(conta0.Saldo, 0.00m);
        }

        [Test]
        public void NaoDeveEfetuarSaqueSuperiorAoSaldoDaConta()
        {
            Conta conta0 = new ContaCorrente();
            conta0.Numero = "233";

            conta0.Agencia = this.agencia;
            conta0.Correntista = this.correntista;

            conta0.Depositar(15.01m);
            // Assert.AreEqual(conta0.Saldo, 15.01m);

            conta0.Sacar(12.04m);
            // Assert.AreEqual(conta0.Saldo, 2.97m);

            conta0.Sacar(2.97m);
            // Assert.AreEqual(conta0.Saldo, 0.00m);

            Assert.Throws<InsufficientFundsException>(() =>
            {
                conta0.Sacar(5.99m);
            });
        }
    }
}