using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solutis.Blog.Core.Domain.Entity
{

    [Table("post_comment")]
    public class Comment
    {
        [Column("id")]
        public long Id { get; set; }

        [ForeignKey("id_blog_post")]
        public Post Post { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("creation")]
        public DateTime CreationDate { get; set; }

        public Comment()
        {
            this.CreationDate = DateTime.Now;
        }
    }
}