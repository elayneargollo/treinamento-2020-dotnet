using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Solutis.Blog.Core.Domain.Entity
{
    [Table("blog_post")]
    public class Post
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("creation")]
        public DateTime CreationDate { get; set; }

        [Column("tags")]
        public string Tags { get; set; }

        public Post()
        {
            this.CreationDate = DateTime.Now;
        }

    }

}