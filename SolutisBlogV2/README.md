```shell
$ dotnet new webapi -o <nome_do_seu_projeto>
``` 

```shell
$ dotnet add package Microsoft.EntityFrameworkCore --version 3.1.10
$ dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 3.1.4
```

// Muitos para muitos
// https://pt.wikipedia.org/wiki/Muitos-para-muitos_(modelagem_de_dados)


Autor {
    List<LivosAutores> LivosAutores
}

Livro {
    List<LivosAutores> LivosAutores
}

LivosAutores {
    Livro Livro;
    Autor Autor;
}

dbContext.Autores.Include((a) => a.LivosAutores).ThenInclude((la) => la.Livro).ToList()
