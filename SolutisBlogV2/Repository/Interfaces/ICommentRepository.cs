using System.Collections.Generic;
using Solutis.Blog.Core.Domain.Entity;

namespace Solutis.Blog.Core.Interfaces
{
    public interface ICommentRepository
    {
        List<Post> GetAll();
        Post GetOne(long id);
        Post Save(Post post);
        void Remove(Post post);
        Post Update(Post post);
        long CountCommentsByPostId(long id);
    }
}