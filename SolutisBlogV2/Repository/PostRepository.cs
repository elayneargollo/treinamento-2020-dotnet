
using System.Collections.Generic;
using System.Linq;
using Solutis.Blog.Core.Domain.Entity;
using Solutis.Blog.Core.Interfaces;
using Solutis.Blog.Data.Config;

namespace Solutis.Blog.Data.Repository
{
    public class PostRepository : IPostRepository
    {
        private DatabaseContext context;

        public PostRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public List<Post> GetAll()
        {
            return context.Posts.ToList();
        }

        public Post GetOne(long id)
        {
            return context.Posts.Where((p) => p.Id == id).SingleOrDefault();
        }

        public void Remove(Post post)
        {
            context.Posts.Remove(post);
            context.SaveChanges();
        }

        public Post Save(Post post)
        {
            context.Posts.Add(post);
            context.SaveChanges();
            return post;
        }

        public Post Update(Post post)
        {
            context.Posts.Update(post);
            context.SaveChanges();
            return post;
        }
    }
}