
using System.Collections.Generic;
using System.Linq;
using Solutis.Blog.Core.Domain.Entity;
using Solutis.Blog.Core.Interfaces;
using Solutis.Blog.Data.Config;

namespace Solutis.Blog.Data.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private DatabaseContext context;

        public CommentRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public long CountCommentsByPostId(long id)
        {
            return context.Comments.Where((c) => c.Post.Id == id).Count();
        }

        public List<Post> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Post GetOne(long id)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(Post post)
        {
            throw new System.NotImplementedException();
        }

        public Post Save(Post post)
        {
            throw new System.NotImplementedException();
        }

        public Post Update(Post post)
        {
            throw new System.NotImplementedException();
        }
    }
}