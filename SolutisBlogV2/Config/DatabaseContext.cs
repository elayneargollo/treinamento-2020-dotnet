
using System;
using Microsoft.EntityFrameworkCore;
using Solutis.Blog.Core.Domain.Entity;

namespace Solutis.Blog.Data.Config
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Console.WriteLine("Contexto Criado");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Console.WriteLine("Modelos Criados");
        }

    }
}


