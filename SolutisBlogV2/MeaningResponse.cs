using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Solutis.Blog.Core.Domain.Models
{

    public class Category
    {

        [JsonPropertyName("label")]
        public string Label { get; set; }

        [JsonPropertyName("relevance")]
        public string Relevance { get; set; }
    }

    public class MeaningResponse
    {

        [JsonPropertyName("category_list")]
        public List<Category> CategoryList { get; set; }

        public string GetTags()
        {
            if (CategoryList != null && CategoryList.Count > 0)
            {
                return CategoryList.Select((c) => c.Label).Aggregate((a, b) => string.Join(",", a, b));
            }
            return string.Empty;
        }

    }
}