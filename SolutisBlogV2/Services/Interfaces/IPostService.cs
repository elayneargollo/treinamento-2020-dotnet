
using System.Collections.Generic;
using Solutis.Blog.Core.Domain.Entity;

namespace Solutis.Blog.Core.Services.Interfaces
{
    public interface IPostService
    {
        List<Post> GetAll();
        Post GetOne(long id);
        Post Save(Post post);
        void Remove(long id);
        long NumberOfComments(long postId);
        Post Update(Post post);
    }
}