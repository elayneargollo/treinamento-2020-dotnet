
```
Postman ou Insomnia
dotnet core sdk v 3.1
VsCode com os plugins [ms-dotnettools.csharp, eamodio.gitlens]
Git SCM
gitkraken (opcional)
Docker (opcional) Se for instalar no windows use WSL2
DBeaver

----- Links -----
Postman         -> https://www.postman.com
Insomnia Core   -> https://insomnia.rest
Dotnet Core SDK -> https://dotnet.microsoft.com/download
VsCode          -> https://code.visualstudio.com
Git             -> https://git-scm.com
GitKraken       -> https://www.gitkraken.com
Docker          -> https://docs.docker.com/engine/install
DBeaver         -> https://dbeaver.io/download/
----- Links -----
```


# Arquitetura inspirada na arquitetura clean

## Criar a pasta principal de seu projeto
```shell
$ mkdir SolutisBlog
$ cd SolutisBlog
```
## Criar os projetos
```shell
$ dotnet new webapi -o Solutis.Blog.Api
$ dotnet new classlib -o Solutis.Blog.Core
$ dotnet new classlib -o Solutis.Blog.Data
$ dotnet new nunit -o Solutis.Blog.Tests
```

## Criar a solution

```shell
$ dotnet new sln
```

## Adicionar os projetos na solution

```shell
$ dotnet sln add Solutis.Blog.Api/Solutis.Blog.Api.csproj
$ dotnet sln add Solutis.Blog.Core/Solutis.Blog.Core.csproj
$ dotnet sln add Solutis.Blog.Data/Solutis.Blog.Data.csproj
$ dotnet sln add Solutis.Blog.Tests/Solutis.Blog.Tests.csproj
```

## Dependências do coverlet [Coberrtura de testes]

> No módulo `Solutis.Blog.Tests` adicionar as dependências para a cobertura de código.

```shell
$ dotnet add package coverlet.collector --version 1.3.0
$ dotnet add package coverlet.msbuild --version 2.9.0
```


## Dependências do EntityFrameworkCore [EF + PostgreSQL]

> Nos módulos `Solutis.Blog.Api` e `Solutis.Blog.Data` adicionar as dependências para o EF eo PostgreSQL.

```shell
$ dotnet add package Microsoft.EntityFrameworkCore --version 3.1.10
$ dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 3.1.4
```


## Link dos projetos de dependência no `Solutis.Blog.Api`

> No módulo `Solutis.Blog.Api` adicionar as referências para os projetos `Solutis.Blog.Core` e `Solutis.Blog.Data`.

```shell
$ dotnet add reference ../Solutis.Blog.Core/Solutis.Blog.Core.csproj
$ dotnet add reference ../Solutis.Blog.Data/Solutis.Blog.Data.csproj
```

## Link do projeto de dependência no `Solutis.Blog.Core`

> No módulo `Solutis.Blog.Core` adicionar a referência para o projeto `Solutis.Blog.Data`.

```shell
$ dotnet add reference ../Solutis.Blog.Data/Solutis.Blog.Data.csproj
```

## Test solution

```shell
$ dotnet clean
$ dotnet build
$ dotnet test
```