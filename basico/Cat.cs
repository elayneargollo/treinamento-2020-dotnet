using System;

namespace Zoo
{
    public class Cat
    {
        public DateTime dataNascimento;
        public string Nome;

        private int QuantdadeDeMiaus = 3;

        public double IdadeDoGatoEmDias()
        {
            return (DateTime.Now - dataNascimento).TotalDays;
        }

        public string Falar(Func<int, string> sotaque) {

            if (sotaque != null)
            {
                QuantdadeDeMiaus++;
                return sotaque(QuantdadeDeMiaus);
            }

            return "meau";
        }

        public Cat()
        {
            this.dataNascimento = DateTime.Now;
        }

        public Cat(string nome) : this()
        {
            this.Nome = nome;
        }

    }
}

