namespace SalaDeAula
{
    public class Aluno
    {
        public string Nome {get; set;}
        public int Idade {get; set;}

        public Aluno(string nome, int idade)
        {
            this.Nome = nome;
            this.Idade = idade;
        }

        public override string ToString()
        {
            return $"{this.Nome}\t{this.Idade}";
        }
    }
}