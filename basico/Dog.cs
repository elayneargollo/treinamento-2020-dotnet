using System;

namespace Zoo
{
    public class Dog
    {
        public DateTime dataNascimento;
        public string Nome;

        public Dog()
        {
            this.dataNascimento = DateTime.Now;
        }

        public Dog(string nome) : this()
        {
            this.Nome = nome;
        }

    }
}

