﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using models;

namespace database_ef
{
    class Program
    {
        public static Context dbContext2 = new Context();
        static void Main(string[] args)
        {

            Console.WriteLine($"{12, 5:D4}Coisas depois");

            Console.WriteLine("Hello World!");


            // Console.WriteLine("Hello World!");
            // http://www.npgsql.org/efcore/

            /*************************************************************
             * Forma automática de inserir um dado e pesquisa-lo no banco
             *************************************************************/

            using (var dbContext = new Context())
            {
                Post post0 = new Post();
                post0.Title = "o meu primeiro post!";
                post0.Content = "é muiito legal fazer inserções no banco de dados.";

                dbContext.Posts.Add(post0);

                Post post1 = new Post();
                post1.Title = "exemplo de um segundo post";
                post1.Content = "alterando a data de criacao para ontem";
                post1.CreationDate = post1.CreationDate.AddDays(-1);

                dbContext.Posts.AddRange(post1);

                int affected = dbContext.SaveChanges();

                Console.WriteLine($"Inserido {affected} posts");
            }


            using (var dbContext = new Context())
            {
                long postId = 12;
                Post post12 = dbContext.Posts.Where((p) => p.Id == postId).FirstOrDefault();

                Console.WriteLine($"{post12.Id.ToString()}, {post12.Title}, {post12.Content}, {post12.CreationDate}");

                post12.Title = post12.Title + " EDITADO";

                dbContext.Update(post12);

                int affected = dbContext.SaveChanges();

                Console.WriteLine($"Updateados {affected} posts");

            }


            using (var dbContext = new Context())
            {
                var posts = dbContext.Posts.OrderBy(p => p.Id);

                foreach (var post in posts)
                {
                    Console.WriteLine($"{post.Id.ToString()}\t{post.Title}\t{post.Content}\t{post.CreationDate}");
                }

            }

            // Quantos posts eu tenho em uma data?
            using (var dbContext = new Context())
            {
                var quantidadeDePosts0 = dbContext.Posts.Where(p => p.CreationDate.Date == DateTime.Today).Count();

                Console.WriteLine("{0} posts de hoje. {1}", quantidadeDePosts0, quantidadeDePosts0 > 0 ? "😎" : "😡");

                var quantidadeDePosts1 = dbContext.Posts.Count(p => p.CreationDate.Date == DateTime.Today);

                Console.WriteLine("{0} posts de hoje. {1}", quantidadeDePosts1, quantidadeDePosts1 > 0 ? "😎" : "😡");

            }

            // Para deletar uma entity:
            using (var dbContext = new Context())
            {
                long idParaDeletar = 14;

                Post postParaDeletar = dbContext.Posts.FirstOrDefault((p) => p.Id == idParaDeletar);

                int affecteds = 0;
                if (postParaDeletar != null)
                {
                    var posts = dbContext.Remove(postParaDeletar);
                    affecteds = dbContext.SaveChanges();
                }

                Console.WriteLine($"{affecteds} posts deletados");

            }


            /*************************************************************
             * Quantidade de posts por dia [Custom Query]
             *************************************************************/

            // https://stackoverflow.com/questions/35631903/raw-sql-query-without-dbset-entity-framework-core/35633596

            // Singleton Context
            Func<Context> func = () =>
            {
                if (dbContext2 != null)
                {
                    dbContext2 = new Context();
                }
                return dbContext2;
            };

            using (var dbContext = new Context())
            {
                var command = dbContext.Database.GetDbConnection().CreateCommand();
                command.CommandText = @"
                    select 
                        date_trunc('day', creation) as day,
                        count(*) as qty
                    from blog_post
                    group by date_trunc('day', creation)
                    order by date_trunc('day', creation)";

                command.CommandType = CommandType.Text;

                dbContext.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    int dayPosition = result.GetOrdinal("day");
                    int qtyPosition = result.GetOrdinal("qty");

                    while (result.Read())
                    {
                        Console.WriteLine($"{result.GetDateTime(dayPosition).ToString("dd/MM/yyyy"),-20}{result.GetInt64(qtyPosition)}");
                    }
                }
            }

            // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/basic-linq-query-operations
            /*************************************************************
             * Quantidade de posts por dia [Custom Query - LinQ]
             *************************************************************/
            Console.WriteLine("--------------------------");
            using (var dbContext = new Context())
            {
                var query = (from p in dbContext.Posts
                             group p by p.CreationDate.Date into g
                             select new
                             {
                                 day = g.Key,
                                 qty = g.Count()
                             }).OrderBy(o => o.day);

                var lista = query.ToList();

                foreach (var i in lista)
                {
                    Console.WriteLine($"{i.day.ToString("dd/MM/yyyy"),-20}{i.qty}");
                }

            }

            // Faça com link Imperativo a Quary de group by.
            // Mapeem PostComment e ligue com o BlogPost

            // Dica ForeignKey
            // public List<Comment> Comments { get; set; }


            /*************************************************************
             * Quantidade de posts por dia [Custom Query - LinQ]
             *************************************************************/
            using (var db = new Context())
            {
                var _groups = db.Posts.GroupBy(p => p.CreationDate.Date).Select(g => new
                {
                    day = g.Key,
                    qty = g.Count()
                }).OrderBy(o => o.day);

                foreach (var grp in _groups)
                {
                    Console.WriteLine("Day: {0} - Quantidade: {1}", grp.day, grp.qty);
                }
            }



            using (var db = new Context())
            {
                Post post = db.Posts.SingleOrDefault((p) => p.Id == 20);

                if (post != null)
                {
                    Console.WriteLine(post.Title);
                    Console.WriteLine(post.Content);

                    Comment comment = new Comment
                    {
                        Content = "não acho que seja muuito legal não.",
                        Post = post
                    };

                    db.Comments.Add(comment);

                    int affected = db.SaveChanges();
                    Console.WriteLine(affected);
                }
            }



            using (var db = new Context())
            {
                Post post = db.Posts.SingleOrDefault((p) => p.Id == 20);

                if (post != null)
                {
                    Console.WriteLine(post.Title);
                    Console.WriteLine(post.Content);

                    var comentario = new Comment
                    {
                        Content = "Comentarios adicionados pela lista!!",
                        Post = post
                    };

                    post.Comments = new List<Comment>();
                    post.Comments.Add(comentario);

                    db.Posts.Update(post);

                    int affected = db.SaveChanges();
                    Console.WriteLine(affected);

                }
            }

            /*************************************************************
             * Query iniciando pelo post
             *************************************************************/
            using (var db = new Context())
            {
                List<Post> posts = db.Posts.Include(p => p.Comments).ToList();
                    // .Where(p => p.Id == 20).ToList();

                foreach (var p in posts)
                {
                    Console.WriteLine($"{p.Id,-5:D3}{p.Comments.Count(),-5}{p.CreationDate.ToString("dd/MM/yyyy HH:mm:ss"),-20}{p.Content}");
                }
            }


            /*************************************************************
             * Query iniciando pelo Comment
             *************************************************************/
            using (var db = new Context())
            {
                List<Comment> comments = db.Comments
                    .Include(c => c.Post)
                    .Where(c => c.Post.Id == 20).ToList();

                foreach (var c in comments)
                {
                    Console.WriteLine($"{c.Id,-5:D3}{c.CreationDate.ToString("dd/MM/yyyy HH:mm:ss"),-20}{c.Content}");
                }
            }

            /*************************************************************
             * Quantidade de comentarios de cada post
             *************************************************************/

            using (var db = new Context())
            {
                var groups = db.Comments.Include(c => c.Post)
                    .GroupBy(c => c.Post.Id).Select(g => new {
                        PostId = g.Key,
                        Quantity = g.Count()
                    });

                foreach (var g in groups)
                {
                    Console.WriteLine($"{g.PostId, -6:D5}{g.Quantity, 6}");
                }

            }



        }
    }
}
