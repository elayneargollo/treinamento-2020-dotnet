using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace models
{

    [Table("post_comment")]
    public class Comment
    {
        [Column("id")]
        public long Id { get; set; }

        // [Column("id_blog_post")]
        // public long PostId { get; set; }

        [ForeignKey("id_blog_post")]
        public Post Post { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("creation")]
        public DateTime CreationDate { get; set; }

        public Comment()
        {
            this.CreationDate = DateTime.Now;
        }
    }
}