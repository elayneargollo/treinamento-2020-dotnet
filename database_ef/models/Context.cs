using System;
using Microsoft.EntityFrameworkCore;

namespace models
{
    public class Context : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(@"Host=postgres.evilsummit.com.br;Port=5432;Username=usr097;Password=zbdUKJ3Ltc;Database=db097;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Console.WriteLine("Modelo Criado");
        }
    }
}