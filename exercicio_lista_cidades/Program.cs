﻿using System;
using System.Collections.Generic;
using System.Linq;
using Solutis;
using Solutis.FileProcessing;

namespace exercicio_lista_cidades
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Dictionary<string, int> pessoasPorUF1 = new Dictionary<string, int>();

            Action<int, Municipio> action = (idx, municipio) =>
            {
                if (pessoasPorUF1.ContainsKey(municipio.UF))
                {
                    pessoasPorUF1[municipio.UF] += municipio.Populacao;
                }
                else
                {
                    pessoasPorUF1.Add(municipio.UF, municipio.Populacao);
                }
            };

            new FileReader("Lista_Municípios_com_IBGE_Brasil_Versao_CSV.csv").ReadFile(action);

            foreach (var item in pessoasPorUF1)
            {
                Console.Out.WriteLine(item.Key + "\t" + item.Value);  
            }

            // 

            long quanidade0 = new FileReader("Lista_Municípios_com_IBGE_Brasil_Versao_CSV.csv").ReadFile()
                .Where((m) => {
                    return String.Equals(m.UF, "BA", StringComparison.InvariantCultureIgnoreCase);
                })
                .Count();

            Console.WriteLine(quanidade0);

            long quanidade2 = new FileReader("Lista_Municípios_com_IBGE_Brasil_Versao_CSV.csv").ReadFile()
                .Count((m) => {
                    return String.Equals(m.UF, "BA", StringComparison.InvariantCultureIgnoreCase);
                });

            Console.WriteLine(quanidade2);

            // Faça um Dictionary contendo O UF e a populaçao.
            // BA -> 32444 habitantes

            // Dica: Aggregate e GroupBy podem ser uteis.

            IEnumerable<Municipio> municipios2 = new FileReader("Lista_Municípios_com_IBGE_Brasil_Versao_CSV.csv").ReadFile();

            var populacaoPorUf2 = municipios2.GroupBy(
                (municipio) => municipio.UF, // Key
                (municipio) => municipio.Populacao, // Projecao
                (uf, agg) => new // Agregador
                {
                    Key = uf,
                    Count = agg.Count(),
                    Min = agg.Min(),
                    Max = agg.Max(),
                    Sum = agg.Sum()
                }).ToList();
            
            foreach (var item in populacaoPorUf2)
            {
                Console.Out.WriteLine(item);  
            }



            Pessoa p1 = new Pessoa();
            p1.Nome = "Davi";

            Pessoa p2 = new Pessoa();
            p2.Nome = "Davi";

            Pessoa p3 = new Pessoa();
            p3.Nome = "Ângela";

            Console.WriteLine("essas pessoas so iguais? " + (p1 == p2));
            Console.WriteLine("essas pessoas so iguais? " + (p1.Equals(p2)));

            Console.WriteLine("a soma dessas pessoas é igual a " + (p1 + p3));

            try
            {
                Console.WriteLine("essas pessoas so iguais? " + (p1 - p3));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            finally
            {
                Console.WriteLine("Tá tudo bem agora, o pior já passou!");
            }

        }
    }
}
