using System;

namespace Solutis.FileProcessing
{
    public class Municipio
    {
        private char Separator = Constants.DEFAULT_SEPARATOR;
        private const int ExpectedFieldLength = 10;

        private const int PosicaoNome = 4;
        private const int PosicaoUF = 3;
        private const int PosicaoPopulacao = 6;
        private const int PosicaoCapital   = 8;

        // ConcatUF+Mun;IBGE;IBGE7;UF;Município;Região;População 2010;Porte;Capital;
        public string Source { get; }
        public string[] SourceFields { get; }

        public string Nome { get => SourceFields[PosicaoNome]; }
        public string UF { get => SourceFields[PosicaoUF]; }
        public bool Capital { get => string.Equals(SourceFields[PosicaoCapital].Trim(), "Capital", StringComparison.InvariantCultureIgnoreCase); }

        public int Populacao
        {
            get
            {
                string fld = SourceFields[PosicaoPopulacao].Trim();

                if (string.IsNullOrWhiteSpace(fld))
                {
                    return 0;
                }

                return Int32.Parse(fld);
            }
        }

        public Municipio(string source)
        {

            if (string.IsNullOrWhiteSpace(source))
            {
                throw new IncorrectParameterException($"Empty line");
            }

            this.Source = source;

            SourceFields = this.Source.Split(Separator);

            if (SourceFields.Length != ExpectedFieldLength)
            {
                throw new IncorrectParameterException($"CsV Fields incorrect size, expected {ExpectedFieldLength} actual {SourceFields.Length}");
            }
        }

        public override string ToString()
        {
            return $"{Nome}\t{UF}\t{Populacao}\t{Nome}\t{Capital}";
        }
    }
}