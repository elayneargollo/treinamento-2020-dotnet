using System;
using System.Collections.Generic;
using System.IO;

namespace Solutis.FileProcessing
{
    public class FileReader
    {
        private string fileName;

        public FileReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<Municipio> ReadFile()
        {
            int counter = 0;

            StreamReader file = new StreamReader(this.fileName);

            string line;

            while ((line = file.ReadLine()) != null)
            {
                yield return new Municipio(line);
                counter++;
            }

            file.Close();

            System.Console.WriteLine("There were {0} lines.", counter);

        }

        public void ReadFile(Action<int, Municipio> action)
        {

            if (action == null)
            {
                throw new IncorrectParameterException("Action must an not null argument");
            }

            int counter = 0;

            StreamReader file = new StreamReader(this.fileName);

            string line;

            while ((line = file.ReadLine()) != null)
            {
                action(counter, new Municipio(line));
                counter++;
            }

            file.Close();

            System.Console.WriteLine("There were {0} lines.", counter);
        }
    }
}
