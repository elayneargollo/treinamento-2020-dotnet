using System;
using System.Runtime.Serialization;

namespace Solutis.FileProcessing
{
    [Serializable]
    internal class IncorrectParameterException : Exception
    {
        public IncorrectParameterException(string message) : base(message)
        {            
        }
    }
}